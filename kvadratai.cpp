#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

int main() {
    ifstream data("duom.txt");
    ofstream rez("rez.txt"); 
    
    int skaicius;
    data >> skaicius;
    
    
    int naujasSkaicius = 0;
    int laipsnis = 1;
    
    while (skaicius > 0) {
        int skaitmuo = skaicius % 10;
        naujasSkaicius += std::pow(skaitmuo, 2) * laipsnis;
        skaicius /= 10;
        laipsnis *= 10;
    }
    
    
    
    rez << naujasSkaicius << endl;
   
    data.close();
    rez.close();
    
    
    return 0;
}

