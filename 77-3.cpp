#include <iostream>

using namespace std;

int main() {
  int n, k;
  k = 0;
  cin >> n;
  while (n > 0) {
    if (n % 2 == 0) {
      n = n / 2;
    }

    else if (n % 2 == 1) {
      n--;
    }
    k++;
  }
  cout << k << endl;
  return 0;
}
