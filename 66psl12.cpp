#include <iostream>

using namespace std;

int main() {
  int n;
  std::cout << "Įveskite skaičių: ";
  std::cin >> n;

  int naujas_skaicius = 0;
  int skaitmuo;

  while (n != 0) {
    skaitmuo = n % 10; // Gauname skaitmenį
    if (skaitmuo % 2 == 0) {
      naujas_skaicius =
          naujas_skaicius * 10 +
          skaitmuo; // Pridedame lyginį skaitmenį prie naujo skaičiaus
    }
    n /= 10; // Pasišaliname skaitmenį iš pradinio skaičiaus
  }
  // Reversing the digits in the new number
  int reversed_skaicius = 0;
  while (naujas_skaicius != 0) {
    skaitmuo = naujas_skaicius % 10;
    reversed_skaicius = reversed_skaicius * 10 + skaitmuo;
    naujas_skaicius /= 10;
  }

  std::cout << "Naujas skaičius tik iš lyginių skaitmenų: " << reversed_skaicius
            << std::endl;

  return 0;
}
