#include <iostream>

using namespace std;

int main() {
  int visi = 0;
  int pateko = 0;
  int kiekreik;

  while (true) {
    cout << "Įveskite kiek balų gavo mokinys: ";
    cin >> kiekreik;

    if (kiekreik == 0) {
      break;
    }

    visi++;

    if (kiekreik >= 5) {
      pateko++;
    }
  }

  cout << endl;
  cout << "rezultatai" << endl;
  cout << endl;
  cout << "Į karnavalą ėjo " << visi << " mokiniai, pateko " << pateko << endl;

  return 0;
}
