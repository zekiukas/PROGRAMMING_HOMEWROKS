# Makefile for C++ Competitive Programming

# Compiler and compiler flags
CXX = g++
CXXFLAGS = -std=c++17 -O2 -Wall

# Source files and executables
SRCS := $(wildcard *.cpp)
EXECS := $(patsubst %.cpp,%,$(SRCS))

# Build all executables
all: $(EXECS)

%: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ $<

run: $(EXECS)
	@for exe in $(EXECS); do \
		echo "Running $$exe..."; \
		./$$exe; \
	done

clean:
	rm -f $(EXECS)

.PHONY: all run clean

