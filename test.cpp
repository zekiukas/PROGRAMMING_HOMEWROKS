#include <iostream>

int nuosekliPaieska(int arr[], int dydis, int ieskomasElementas) {
    for (int i = 0; i < dydis; ++i) {
        if (arr[i] == ieskomasElementas) {
            return i; // jeigu rado elementą, gražina jo vietą masyve
        }
    }
    return -1; // jeigu elemento nerado, gražina -1
}

int main() {
    int arr[] = {1, 3, 5, 7, 9, 11};
    int dydis = sizeof(arr) / sizeof(arr[0]); // suskaičiuoja masyvo dydį (kad žinotų kiek kartų kartoti ciklą)
    int ieskomasElementas = 5; // elementas kurio ieškome
    int rez = nuosekliPaieska(arr, dydis, ieskomasElementas);

    if (rez != -1) {
        std::cout << "elementas " << ieskomasElementas << " buvo rastas čia: " << rez << std::endl;
    } else {
        std::cout << "elemento " << ieskomasElementas << " masyve nebuvo" << std::endl;
    }

    return 0;
}

