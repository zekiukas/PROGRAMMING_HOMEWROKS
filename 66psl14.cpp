#include <iostream>

using namespace std;

int main() {
  int n, k;
  cout << "Įveskite skaičių n: ";
  cin >> n;
  cout << "Įveskite skaitmenį k: ";
  cin >> k;

  int count = 0;
  while (n > 0) {
    int digit = n % 10;
    n = n / 10;
    if (digit > k) {
      count++;
    }
  }

  cout << "Skaitmenų, didesnių už " << k << " yra " << count << endl;

  return 0;
}
