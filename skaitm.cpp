#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

int main() {
    

    ifstream data("duom.txt");
    ofstream rez("rez.txt");


    int skaicius, sk, lyg = 0, sum = 0, isviso = 0; 

    data >> skaicius;



   while(skaicius > 0) {
        sk = skaicius % 10;
        skaicius = skaicius / 10;
        if(sk != 0 && sk % 2 == 0) {
            lyg++;
        }
        sum += sk;
        isviso++;
    }
    
    auto vid = (double)sum / isviso;

    rez << fixed << setprecision(2) << vid << endl << lyg << endl;

    data.close();
    rez.close();


    return 0;

}
