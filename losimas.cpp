#include <iostream>
#include <fstream>
#include <vector>

using namespace std;


int main() {
    ifstream data("taskai.txt");
    ofstream rez("taskairez.txt");

    std::vector<int> duomenys;
    int skaicius;
    while (data >> skaicius) {
        duomenys.push_back(skaicius);
    }

    data.close();

    int geriausias_zaidejas = -1;
    int geriausio_zaidejo_taskai = 0;

    for (int i = 0; i < duomenys.size(); ++i) {
        int zaidejo_taskai = duomenys[i];
        int taskai = 0;

        while (zaidejo_taskai % 2 == 0) {
            taskai += zaidejo_taskai;
            zaidejo_taskai = 0;
        }

        if (taskai > geriausio_zaidejo_taskai) {
            geriausio_zaidejo_taskai = taskai;
            geriausias_zaidejas = i + 1;
        }
    }


    for (int taskai : duomenys) {
        rez << taskai << " ";
    }

    rez << endl << geriausias_zaidejas;

    rez.close();


    return 0;
}

