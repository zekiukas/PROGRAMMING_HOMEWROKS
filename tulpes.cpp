#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

int main() {

    ifstream file("duom.txt");
    ofstream rez("rez.txt");

    int n, t, k, dienos = 0, temp;

    file >> n >> t >> k;

    if(n >= 0) {
        n = n - t;
        dienos++;
    }

    while(n > 0) {
        t += k;
        n = n - t;
        dienos++;

    }


    rez << dienos << "\n";

    file.close();
    rez.close();


    return 0;
}
