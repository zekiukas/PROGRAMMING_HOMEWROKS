#include <fstream>
#include <cmath>
#include <iostream>
#include <iomanip>

using namespace std;

int main() {

    ifstream data("duom.txt");
    ofstream rez("rez.txt");
   
    double mp, mg, z, y;

    data >> mp >> mg >> z;

    double m = mp;

    while(m <= mg) {
       y =  (m + 3) / sqrt(pow(m, 2) - 100);
        if(isfinite(y)) {
       rez << fixed << setprecision(2) << setw(8) << m << " ";
       rez << fixed << setprecision(3) << setw(8) << y << endl;
        }
       m = m + z;

    }





    data.close();
    rez.close();




    return 0;
}
