#include <fstream>
#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

int main() {

    ifstream data("duom.txt");
    ofstream rez("rez.txt");
    
    string number;

    while (data >> number) {
        std::reverse(number.begin(), number.end());
        rez << number << endl;
    }


    data.close();
    rez.close();

    return 0;
}
