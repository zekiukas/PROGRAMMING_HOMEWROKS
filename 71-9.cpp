#include <iostream>

using namespace std;

int main() {
  int pr = 5, pb = 25, sk, sum = 0;
  while (true) {
    cin >> sk;
    if (sk == 0)
      break;

    if (sk > pr && sk < pb)
      sum += sk;
  }

  cout << sum << endl;
  return 0;
}
